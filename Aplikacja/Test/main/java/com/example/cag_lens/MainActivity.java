package com.example.cag_lens;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements SensorEventListener, PopupMenu.OnMenuItemClickListener {
    private Camera mCamera;
    private SensorManager mSensorManager;
    private Sensor mRotationV, mAccelerometer, mMagnetometer;
    public CameraView mPreview;
    public float[] click_values;
    private float height;
    private float width;
    Compass compassActivity;
    FusedLocationProviderClient fusedLocationProviderClient;
    LocationRequest locationRequest;
    private static final int LOCATION_REQUEST_CODE = 100;
    double myLatitude, myLongitude=0, myBearing=0, oldLatitude=0, oldLongitude=0, speed = 0;
    private boolean isNearbySearchModeOn = false;
    private String nearbyPlaces = "  Tryb wylaczony";
    private boolean isCurrentLocalizationUpdateModeOn = true;
    ArrayList<LatLng> hints_points = new ArrayList();
    ArrayList<String> hints_list = new ArrayList();
    double threshold = 50;
    TextToSpeech textToSpeech;
    MinimapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        onWindowFocusChanged(true);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setInterval(4000);
        locationRequest.setFastestInterval(2000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        startButtonClickListeners();
        startSensors();
        compassActivity = new Compass(MainActivity.this);

        mapFragment = (MinimapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapView);

	textToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status == TextToSpeech.SUCCESS) {
                    textToSpeech.setLanguage(Locale.ENGLISH);
                }
            }
        });

        startCamera();
    }




    public static double distance(LatLng point1, LatLng point2) {

        final int R = 6371;

        double latDistance = Math.toRadians(point2.latitude - point1.latitude);
        double lonDistance = Math.toRadians(point2.longitude - point1.longitude);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(point1.latitude)) * Math.cos(Math.toRadians(point2.latitude))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000;


        distance = Math.pow(distance, 2);

        return Math.sqrt(distance);
    }


    // ================== Funkcje przycisków ==================
    private void startButtonClickListeners() {
        ImageButton btnSettings = findViewById(R.id.settingsButton);
        ImageButton btnMap = findViewById(R.id.mapButton);
        ImageButton btnMinimap = findViewById(R.id.minimapButton);
        ImageButton btnObjectInfo = findViewById(R.id.objectInfoButton);
        ImageButton testBtnSpeak = findViewById(R.id.btnSpeak);


        testBtnSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSpeech(v);
            }
        });

        btnObjectInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                showPlaces();
            }

        });

        btnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(MainActivity.this, v);
                popup.setOnMenuItemClickListener(MainActivity.this);
                popup.inflate(R.menu.menu_fragment);

                MenuItem item1 = popup.getMenu().findItem(R.id.nearby_search_mode);
                if (isNearbySearchModeOn)
                    item1.setTitle("Nearby search mode OFF");
                else
                    item1.setTitle("Nearby search mode ON");

                MenuItem item2 = popup.getMenu().findItem(R.id.current_localization_update_mode);
                if (isCurrentLocalizationUpdateModeOn)
                    item2.setTitle("Current localization update OFF");
                else
                    item2.setTitle("Current localization update ON");

                popup.show();
            }
        });

        btnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMap();
            }
        });

        btnMinimap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                minimapHandler("click");
            }
        });
    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {
        Toast.makeText(this, "Selected Item: " +item.getTitle(), Toast.LENGTH_SHORT).show();
        switch (item.getItemId()) {
            case R.id.compass_calibration:
                // Compass Calibration
                compassActivity.calibrate(false);
                return true;
            case R.id.undo_compass_calibration:
                // Undo Compass Calibration
                compassActivity.calibrate(true);
                return true;
            case R.id.nearby_search_mode:
                // Nearby search mode toggle
                if (isNearbySearchModeOn) {
                    isNearbySearchModeOn = false;}
                else {
                    isNearbySearchModeOn = true;}
                Log.d("Nearby search mode: ", String.valueOf(isNearbySearchModeOn));
            case R.id.current_localization_update_mode:
                // Current localization update mode toggle
                if (isCurrentLocalizationUpdateModeOn) {
                    isCurrentLocalizationUpdateModeOn = false;}
                else {
                    isCurrentLocalizationUpdateModeOn = true;}
                Log.d("CLUM: ", String.valueOf(isCurrentLocalizationUpdateModeOn));
            default:
                return false;
        }
    }

    // ========== Obecna lokalizacja na bieżąco ===================

    @Override
    protected void onStart() {
        super.onStart();
        if(isCurrentLocalizationUpdateModeOn){

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                checkSettingsAndStartLocationUpdates();
            } else {
                askLocationPermission();
            }

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(isCurrentLocalizationUpdateModeOn){
            stopLocationUpdates();
        }

    }

    private void showMap() {
        Intent intent = new Intent(MainActivity.this, MapsActivity.class);

        Double l1 = myLatitude;
        Double l2 = myLongitude;
        String latitude = l1.toString();
        String longitude = l2.toString();

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", getApplicationContext().MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("myLatitude", latitude);
        editor.putString("myLongitude", longitude);
        Log.d("onClick: ", myLatitude+"");
        editor.apply();

        startActivity(intent);
    }

    private void showPlaces() {
        Intent intent = new Intent(MainActivity.this, ObjectInfo.class);
        intent.putExtra("MAIN_ACTIVITY", 1);
        intent.putExtra("INFO", nearbyPlaces);
        startActivity(intent);
    }

    private void minimapHandler(String mode) {
        View minimap = findViewById(R.id.minimapFragment);
        View minimap_bg = findViewById(R.id.minimapBgView);
        if (mode.equals("click")) {
            if (minimap.getVisibility() == View.VISIBLE) {
                minimap.setVisibility(View.INVISIBLE);
                minimap_bg.setVisibility(View.INVISIBLE);
            }
            else {
                minimap.setVisibility(View.VISIBLE);
                minimap_bg.setVisibility(View.VISIBLE);
            }
        }
        else if (mode.equals("hide")) {
            minimap.setVisibility(View.INVISIBLE);
            minimap_bg.setVisibility(View.INVISIBLE);
        }
        else if (mode.equals("show")) {
            minimap.setVisibility(View.VISIBLE);
            minimap_bg.setVisibility(View.VISIBLE);
        }
    }

    private static long calculateDistance(double lat1, double lng1, double lat2, double lng2) {
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        long distanceInMeters = Math.round(6371000 * c);
        return distanceInMeters;
    }

    private void checkSettingsAndStartLocationUpdates() {
        LocationSettingsRequest request = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest).build();
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> locationSettingsResponseTask = client.checkLocationSettings(request);
        locationSettingsResponseTask.addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                startLocationUpdates();
            }
        });
        locationSettingsResponseTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    ResolvableApiException apiException = (ResolvableApiException) e;
                    try {
                        apiException.startResolutionForResult(MainActivity.this, 1001);
                    } catch (IntentSender.SendIntentException sendIntentException) {
                        sendIntentException.printStackTrace();
                    }
                }
            }
        });
    }

    LocationCallback locationCallback = new LocationCallback() {
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onLocationResult(LocationResult locationResult) {
            if (locationResult == null) {
                return;
            }
            for (Location location : locationResult.getLocations()) {
                Log.d("LOCATION", location.toString());

                if(Math.abs(myLatitude-location.getLatitude()) >=0.001 || Math.abs(myLongitude-location.getLongitude())>=0.001){
                    oldLatitude = myLatitude;
                    oldLongitude = myLongitude;
                    myLatitude = location.getLatitude();
                    myLongitude = location.getLongitude();
                    double y = Math.sin(myLongitude-oldLongitude)*Math.cos(myLatitude);
                    double x = Math.cos(oldLatitude) * Math.sin(myLatitude) - Math.sin(oldLatitude)*Math.cos(myLatitude)*Math.cos(myLongitude-oldLongitude);
                    double fi = Math.atan2(y,x);
                    myBearing = (fi*180/Math.PI +360)%360;
                }

                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", getApplicationContext().MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();

                myLatitude = location.getLatitude();
                myLongitude = location.getLongitude();
                speed = location.getSpeed();
                Double l1 = myLatitude;
                Double l2 = myLongitude;
                String latitude = l1.toString();
                String longitude = l2.toString();
                editor.putString("myLatitude", latitude);
                editor.putString("myLongitude", longitude);


                Double b = myBearing;
                String bearing = b.toString();
                ArrayList<Double> distances = new ArrayList<>();
                editor.putString("myBearing", bearing);
                editor.apply();
                Log.d("BEARING", b.toString());
                if (hints_points.size()>0) {
                    int index = 0;
                    LatLng myLocation = new LatLng(myLatitude, myLongitude);
                    LatLng point = hints_points.get(index);
                    double distance = distance(point, myLocation);

                    if (Math.abs(distance) < threshold ) {
                        speakText(hints_list.get(index));
                        hints_list.remove(index);
                        hints_points.remove(index);
                    }
                }

            }

            // W tym miejscu są pobierane wartości checkBoxów
            // Jeśli żaden nie jest zaznaczony to nie zawężamy kryteriów
            // Jeśli wszystkie są zaznaczone zawęzamy kryteria dla wszystkich zaznaczonych

            boolean university = getSharedPreferences("Preferences", Context.MODE_PRIVATE).getBoolean("university", false);
            boolean restaurant = getSharedPreferences("Preferences", Context.MODE_PRIVATE).getBoolean("restaurant", false);
            boolean school = getSharedPreferences("Preferences", Context.MODE_PRIVATE).getBoolean("school", false);
            boolean nightclub = getSharedPreferences("Preferences", Context.MODE_PRIVATE).getBoolean("nightclub", false);
            boolean pharmacy = getSharedPreferences("Preferences", Context.MODE_PRIVATE).getBoolean("pharmacy", false);
            boolean park = getSharedPreferences("Preferences", Context.MODE_PRIVATE).getBoolean("park", false);
            boolean library = getSharedPreferences("Preferences", Context.MODE_PRIVATE).getBoolean("library", false);
            boolean shoppingmall = getSharedPreferences("Preferences", Context.MODE_PRIVATE).getBoolean("shoppingmall", false);
            boolean postoffice = getSharedPreferences("Preferences", Context.MODE_PRIVATE).getBoolean("postoffice", false);
            boolean supermarket = getSharedPreferences("Preferences", Context.MODE_PRIVATE).getBoolean("supermarket", false);

            ArrayList<String> checkedTypes = new ArrayList<String>();
            if (university) checkedTypes.add("university");
            if (restaurant) checkedTypes.add("restaurant");
            if (school) checkedTypes.add("school");
            if (nightclub) checkedTypes.add("night_club");
            if (pharmacy) checkedTypes.add("pharmacy");
            if (park) checkedTypes.add("park");
            if (library) checkedTypes.add("library");
            if (shoppingmall) checkedTypes.add("shopping_mall");
            if (postoffice) checkedTypes.add("post_office");
            if (supermarket) checkedTypes.add("supermarket");

            // informacje o miejscach w promieniu 100 metrów od aktualnej lokalizacji
            if (isNearbySearchModeOn) {
                nearbyPlaces = "";
                // Osobne zapytanie dla kazdego wybranego typu - zgodnie z dokumentacja nie mozna dawac wielu typow w jednym zapytaniu
                for (String type : checkedTypes)
                    new getNearbyAsyncTask(myLatitude, myLongitude, 100, type).execute();
                if (checkedTypes.isEmpty())
                    new getNearbyAsyncTask(myLatitude, myLongitude, 100, "").execute();
            } else {
                Log.d("Nearby places", "Disabled");
            }
        }
    };

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
    }

    private void stopLocationUpdates(){
        fusedLocationProviderClient.removeLocationUpdates(locationCallback);
    }

    private void askLocationPermission(){
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)==PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)){
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
            }else{
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode==LOCATION_REQUEST_CODE){
            if(grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){
                checkSettingsAndStartLocationUpdates();
            }
        }
    }
    // ================== Funkcje kamery ==================
    private void startCamera() {
        if (checkCameraPermission()) {
            mCamera = getCameraInstance();
            mPreview = new CameraView(this, mCamera);
            FrameLayout preview = findViewById(R.id.camera_preview);
            preview.removeAllViews();
            preview.addView(mPreview);
        }
        else
            requestCameraPermission();
    }


    private boolean checkCameraPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }


    private void requestCameraPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA},
                200);
    }


    public static Camera getCameraInstance()
    //  Metoda, która otwiera i zwraca instancję kamery albo wyświetla błąd, jeśli nie może jej otworzyć
    {
        Camera c = null;
        try {
            c = Camera.open();
        }
        catch(Exception e) {
            Log.e("Camera", "Camera cant be opened " + e);
        }
        return c;
    }


    // ================== Funkcje sensorów ==================
    private void startSensors() {
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if(mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) == null || mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) == null) {
            noSensorAlert();
        }
        else {
            mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            mMagnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
            mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
            mSensorManager.registerListener(this, mMagnetometer, SensorManager.SENSOR_DELAY_UI);

        }
        if(mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR) != null) {
            mRotationV = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
            mSensorManager.registerListener(this, mRotationV, SensorManager.SENSOR_DELAY_UI);
        }
    }


    public void onSensorChanged(SensorEvent event) {
        compassActivity.sensorChanged(mSensorManager, event);
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }


    public void noSensorAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setMessage("Your device doesn't support the compass.")
                .setCancelable(false)
                .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
    }


    // =================== Inne funkcje ===================
    public boolean onTouchEvent(MotionEvent event) {
        // ============= Funkcja odczytująca parametry x i y z klikniętego miejsca i wysyłająca je na serwer================
        int pointerId = event.getPointerId(0);
        int pointerIndex = event.findPointerIndex(pointerId);
        // Get the pointer's current position
        float x = event.getX(pointerIndex);
        float y = event.getY(pointerIndex);

        return false;
    }


    private void convertHints(ArrayList<ArrayList<String>> navigation_hints) {
        hints_points.clear();
        hints_list.clear();
        for (int i = 0; i<navigation_hints.size();i++)
        {
            double lat = Double.parseDouble(navigation_hints.get(i).get(0));
            double lng = Double.parseDouble(navigation_hints.get(i).get(1));
            LatLng point = new LatLng(lat, lng);
            hints_points.add(point);
            hints_list.add(navigation_hints.get(i).get(2));
        }
        Log.d("Hints points: ", String.valueOf(hints_points));
        Log.d("Hints values: ", String.valueOf(hints_list));
    }



    @Override
    protected void onResume() {
        super.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mCamera = null;
        startCamera();
        Intent intent = getIntent();
        Serializable navigation_hints;
        if (intent != null) {
            navigation_hints = intent.getSerializableExtra("navigation_hints");
            ArrayList<ArrayList<String>> hints_array = new ArrayList<>();
            if (navigation_hints != null) {
                hints_array = (ArrayList<ArrayList<String>>) navigation_hints;
                convertHints(hints_array);
            }
        }
    }

    // Klasa odpowiedzialna za wydobycie informacji o obiektach w poblizu
    private class getNearbyAsyncTask extends AsyncTask<Void, Void, String> {
        String sb;
        HttpURLConnection httpURLConnection = null;
        StringBuilder jsonResults = new StringBuilder();
        String t;

        getNearbyAsyncTask(double lat, double lng, int radius, String type) {
             sb = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?" +
                    "location=" + String.valueOf(lat) + "," + String.valueOf(lng) +
                    "&type=" + type +
                    "&radius=" + String.valueOf(radius) +
                    "&key=" + getString(R.string.google_maps_key);
             t = type;
             if (t.equals("")) t = "Wszystkie";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d("OnPreExecute", "running");
        }

        @Override
        protected String doInBackground(Void... params) {
            String myResult = "Nie znaleziono nic w poblizu";

            try {
                URL url = new URL(sb);
                Log.d("Places request", url.toString());
                httpURLConnection = (HttpURLConnection) url.openConnection();
                InputStreamReader in = new InputStreamReader(httpURLConnection.getInputStream());

                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }

            } catch (MalformedURLException e) {
                Log.e("Places", "Error processing Places API URL", e);
            } catch (IOException e) {
                Log.e("Places", "Error connecting to Places API", e);
            } finally {
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
            }

            try {
                JSONObject jsonObj = new JSONObject(jsonResults.toString());
                JSONArray predsJsonArray = jsonObj.getJSONArray("results");

                // Wydobycie informacji o miejscach
                if (predsJsonArray.length() > 0) myResult = "      " + t + ":\n";
                else myResult = "      " + t + ": Brak\n";

                for (int i = 0; i < predsJsonArray.length(); i++) {
                    String name = predsJsonArray.getJSONObject(i).getString("name");
                    String types = predsJsonArray.getJSONObject(i).getString("types");
                    boolean isLocalityFound = types.contains("locality");
                    boolean isPoliticalFound = types.contains("political");

                    if (!isLocalityFound && !isPoliticalFound) {
                        myResult += "        " + name + "\n";
                    }
                }
            } catch (JSONException e) {
                Log.e("Places", "Error processing JSON results", e);
            }

            return myResult;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null) {
                Log.d("NEARBY", result);
                nearbyPlaces += result;
            } else {
                Log.d("NEARBY", "No places nearby");
                nearbyPlaces = "No places nearby";
            }
        }
    }


    // ======================= Rozpoznawanie mowy =======================================
    public void getSpeech(View view) {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "pl");

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 10);
        }
        else {
            Toast.makeText(this, "Twoj telefon nie obsluguje sterowania glosowego", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    processSpeechResult(result);
                }
                break;
        }
    }

    public void processSpeechResult(ArrayList<String> result) {
        String speech = result.get(0).toLowerCase();
        TextView speechView = findViewById(R.id.speechText);
        String[] words = speech.split(" ");

        try {
            if (words[0].matches("prowadź|nawiguj|kieruj") && words[1].matches("do")) {
                String address = speech.split("do ")[1];
                speechView.setText(("Prowadzę do " + address));
            }
            else if (words[0].matches("kalibruj|skalibruj") && speech.contains("kompas")) {
                compassActivity.calibrate(false);
            }
            else if (words[0].matches("anuluj|cofnij|resetuj|zresetuj") && speech.contains("kompas")) {
                compassActivity.calibrate(true);
            }
            else if (speech.contains("obiekty") || speech.contains("okolicy") || speech.contains("pobliżu") || speech.contains("miejsca")) {
                showPlaces();
            }
            else if (words[0].matches("schowaj|ukryj|wyłącz|chowaj")) {
                // Google często nie zna słowa "minimapę", więc parsujemy tak - działa bez zarzutu
                if (((words[1].matches("mi")) && words[2].matches("mapę|mapkę")) || (words[1].matches("minimapę")) || (words[1].matches("minimapkę"))) {
                    minimapHandler("hide");
                }
                if ((words[1].matches("dużą|dużo")) && words[2].matches("mapę|mapkę")) {
                    speechView.setText(("Chowam dużą mapę"));
                }
            }
            else if (words[0].matches("pokaż|włącz")) {
                if (((words[1].matches("mi")) && words[2].matches("mapę|mapkę")) || (words[1].matches("minimapę")) || (words[1].matches("minimapkę"))) {
                    minimapHandler("show");
                }
                if ((words[1].matches("dużą|dużo")) && words[2].matches("mapę|mapkę")) {
                    showMap();
                }
            }
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }





//        View minimap = findViewById(R.id.minimapFragment);
//        View minimap_bg = findViewById(R.id.minimapBgView);
//
//        if (minimap.getVisibility() == View.VISIBLE) {
//            minimap.setVisibility(View.INVISIBLE);
//            minimap_bg.setVisibility(View.INVISIBLE);
//        }
//        else {
//            minimap.setVisibility(View.VISIBLE);
//            minimap_bg.setVisibility(View.VISIBLE);
//        }
    }


    // Funkcja odpowiedzialna za odczytywanie tekstu
    public void speakText(final String text_to_speak) {
        textToSpeech.speak(text_to_speak, TextToSpeech.QUEUE_FLUSH, null);
    }

}
