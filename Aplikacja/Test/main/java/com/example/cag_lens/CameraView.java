package com.example.cag_lens;

import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.ByteArrayOutputStream;


public class CameraView extends SurfaceView implements SurfaceHolder.Callback,Camera.PreviewCallback{
    private SurfaceHolder mSurfHolder;
    private Camera mCamera;
    private int camera_width;
    private int camera_height;
    private MainActivity activity;
    private byte[] camera_data;
    public ByteArrayOutputStream mFrameBuffer;
    private Context context;


    public CameraView(Context s_context, Camera s_camera){
        super(s_context);
        context = s_context;
        mCamera = s_camera;
        mSurfHolder = getHolder();
        mSurfHolder.addCallback(this);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        try {
            mCamera.setPreviewDisplay(holder);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        mCamera.setPreviewCallback(null);
        mCamera.release();
        mCamera = null;
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        try {
            mCamera.stopPreview();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Camera.Parameters parameters = mCamera.getParameters();
            //parameters.setPreviewSize(320,240);
            mCamera.setDisplayOrientation(90);
            camera_width=parameters.getPreviewSize().width;
            camera_height=parameters.getPreviewSize().height;
            parameters.setPreviewFormat(ImageFormat.NV21);

            mCamera.setParameters(parameters);
            mCamera.setPreviewCallback(this);
            mCamera.startPreview();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void onPreviewFrame(byte[] data, Camera camera){
        // Metoda, która konwertuje klatkę obrazu z kamery z YuvImage na JPG.
        camera_data = data;
        YuvImage yuvimage = new YuvImage(camera_data, ImageFormat.NV21, camera_width, camera_height,null);
        ByteArrayOutputStream byte_os = new ByteArrayOutputStream();
        yuvimage.compressToJpeg(new Rect(0,0, camera_width, camera_height), 100, byte_os);
        mFrameBuffer = byte_os;
    }
}
