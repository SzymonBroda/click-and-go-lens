package com.example.cag_lens;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.view.View;
import android.widget.ImageView;

public class Compass {
    ImageView compassImage, directionImage, leftDirectionImage, rightDirectionImage, compassBarImage, targetImage;
    MainActivity mainActivity;
    SharedPreferences sp;
    int mAzimuth, offset, degrees;
    float azimuth, rotation;
    float[] rMat = new float[9];
    float[] rMatRemapped = new float[9];
    float[] orientation = new float[3];
    double myBearing;
    private float[] mLastAccelerometer = new float[3];
    private float[] mLastMagnetometer = new float[3];
    private boolean mLastAccelerometerSet = false;
    private boolean mLastMagnetometerSet = false;

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public Compass(MainActivity mainActivityParent) {
        mainActivity = mainActivityParent;
        compassImage = mainActivity.findViewById(R.id.compassView);
        directionImage = mainActivity.findViewById(R.id.directionView);
        leftDirectionImage = mainActivity.findViewById(R.id.leftDirectionView);
        rightDirectionImage = mainActivity.findViewById(R.id.rightDirectionView);
        compassBarImage = mainActivity.findViewById(R.id.compassBarView);
        targetImage = mainActivity.findViewById(R.id.targetView);
    }


    @SuppressLint({"SetTextI18n", "ResourceType"})
    public void sensorChanged(SensorManager mSensorManager, SensorEvent event) {

        if(event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
            SensorManager.getRotationMatrixFromVector(rMat, event.values);
            SensorManager.remapCoordinateSystem(rMat, SensorManager.AXIS_X, SensorManager.AXIS_Z, rMatRemapped);
            mAzimuth = (int) (Math.toDegrees(SensorManager.getOrientation(rMatRemapped, orientation)[0]))%360;
        }
        if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            System.arraycopy(event.values, 0, mLastAccelerometer, 0, event.values.length);
            mLastAccelerometerSet = true;
        }
        else if(event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            System.arraycopy(event.values, 0, mLastMagnetometer, 0, event.values.length);
            mLastMagnetometerSet = true;
        }
        if(mLastMagnetometerSet && mLastAccelerometerSet) {
            if(mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR) == null) {
                SensorManager.getRotationMatrix(rMat, null, mLastAccelerometer, mLastMagnetometer);
                SensorManager.remapCoordinateSystem(rMat, SensorManager.AXIS_X, SensorManager.AXIS_Z, rMatRemapped);
                SensorManager.getOrientation(rMatRemapped, orientation);
                mAzimuth = (int) ((Math.toDegrees(SensorManager.getOrientation(rMatRemapped, orientation)[0]))%360);
            }
        }
        mAzimuth = Math.round(mAzimuth);
        compassImage.setRotation(-mAzimuth + offset);

        try {
            myBearing = getMyBearing();
        } catch (Exception e) {
            myBearing = 0;
        }

        degrees = mAzimuth - offset;
        if(degrees < 0){
            degrees += 360;
        }

        float trMultiplier = (float) (-getScreenWidth()/60);
        float translation = 0;
        float targetAngle;
        float leftRightOffset = (float) (getScreenWidth()/2.7);
        try {
            targetAngle = calcAngle();
        } catch (Exception e) {
            targetAngle = 999;
        }

        if (degrees >= 337 || degrees <= 22) {
            directionImage.setImageResource(R.drawable.north);
            if (degrees >= 337)
                translation = (degrees-360)*trMultiplier;
            else
                translation = (degrees)*trMultiplier;
        }
        if (degrees < 337 && degrees > 292) {
            directionImage.setImageResource(R.drawable.northwest);
            translation = (degrees-315)*trMultiplier;
            //heading = "NW";
        }
        if (degrees <= 292 && degrees > 247) {
            directionImage.setImageResource(R.drawable.west);
            translation = (degrees-270)*trMultiplier;
            //heading = "W";
        }
        if (degrees <= 247 && degrees > 202) {
            directionImage.setImageResource(R.drawable.southwest);
            translation = (degrees-225)*trMultiplier;
            //heading = "SW";
        }
        if (degrees <= 202 && degrees > 157) {
            directionImage.setImageResource(R.drawable.south);
            translation = (degrees-180)*trMultiplier;
            //heading = "S";
        }
        if (degrees <= 157 && degrees > 112) {
            directionImage.setImageResource(R.drawable.southeast);
            translation = (degrees-135)*trMultiplier;
            //heading = "SE";
        }
        if (degrees <= 112 && degrees > 67) {
            directionImage.setImageResource(R.drawable.east);
            translation = (degrees-90)*trMultiplier;
            //heading = "E";
        }
        if (degrees <= 67 && degrees > 22) {
            directionImage.setImageResource(R.drawable.northeast);
            translation = (degrees-45)*trMultiplier;
            //heading = "NE";
        }

        if (degrees < targetAngle + 22.5 && degrees > targetAngle - 22.5) {
            targetImage.setVisibility(View.VISIBLE);
            targetImage.setTranslationX((degrees-targetAngle)*trMultiplier);
        }
        else {
            targetImage.setVisibility(View.INVISIBLE);

        }

        directionImage.setTranslationX(translation);

        leftDirectionImage.setTranslationX(translation - leftRightOffset);
        if (leftDirectionImage.getTranslationX() < -getScreenWidth()/2.4)
            leftDirectionImage.setVisibility(View.INVISIBLE);
        else
            leftDirectionImage.setVisibility(View.VISIBLE);

        rightDirectionImage.setTranslationX(translation + leftRightOffset);
        if (rightDirectionImage.getTranslationX() > getScreenWidth()/2.4)
            rightDirectionImage.setVisibility(View.INVISIBLE);
        else
            rightDirectionImage.setVisibility(View.VISIBLE);



//        if(mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR) == null) {
//            if(mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) == null || mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) == null) {
//                mainActivity.noSensorAlert();
//            }
//            else {
//                sensorText.setText("Acc + magn");
//            }
//        }
//        else {
//            sensorText.setText("Rot vector");
//        }
    }

    public float calcAngle() {
        sp = mainActivity.getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        double[] myCoords = new double[2];
        double[] targetCoords = new double[2];
        myCoords[0] = Double.parseDouble(sp.getString("myLatitude", ""));
        myCoords[1] = Double.parseDouble(sp.getString("myLongitude", ""));
        targetCoords[0] = Double.parseDouble(sp.getString("latitude", ""));
        targetCoords[1] = Double.parseDouble(sp.getString("longitude", ""));

        double longDif = Math.abs(myCoords[1] - targetCoords[1]);
        double x = Math.cos(targetCoords[0]) * Math.sin(longDif);
        double y = Math.cos(myCoords[0]) * Math.sin(targetCoords[0]) - Math.sin(myCoords[0]) * Math.cos(targetCoords[0]) * Math.cos(longDif);
        double angle = Math.toDegrees(Math.atan2(x, y));
        if (myCoords[1] - targetCoords[1] < 0)
            return (float) -angle;
        else
            return (float) (360+angle);
    }

    public double getMyBearing() {
        sp = mainActivity.getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        double bearing;
        bearing = Double.parseDouble(sp.getString("myBearing", ""));
        return bearing;
    }

    public void calibrate(Boolean undo) {
        if(!undo)
            offset = mAzimuth;
        else
            offset = 0;
    }
}
