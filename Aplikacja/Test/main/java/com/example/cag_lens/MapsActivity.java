package com.example.cag_lens;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MapsActivity extends FragmentActivity {
    private ImageButton btnBack;
    AutoCompleteTextView editText;
    private Context mContext;
    LatLng currentLatLng;
    private boolean firstTime = true;
    ArrayList<ArrayList<String>> navigation_hints;
    ArrayList<LatLng> hints_points = new ArrayList();
    ArrayList<String> hints_list = new ArrayList();
    MinimapFragment mapFragment;


    private static final int REQUEST_CODE = 101;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(MapsActivity.this, MainActivity.class);
        intent.putExtra("navigation_hints",navigation_hints);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        mapFragment = (MinimapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapView);

        mContext = getApplicationContext();
        btnBack = findViewById(R.id.mapsBackButton);

        btnBack.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MapsActivity.this, MainActivity.class);
                        intent.putExtra("navigation_hints",navigation_hints);
                        startActivity(intent);
                    }
                });

        // ======================================================================
        //Assign variable
        editText = (AutoCompleteTextView) findViewById(R.id.edit_text);

        //Set EditText non Focusable
        editText.setFocusable(false);

        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Place.Field> fieldList = Arrays.asList(Place.Field.ADDRESS,
                        Place.Field.LAT_LNG, Place.Field.NAME);
                //Create intent

                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY,
                        fieldList).setInitialQuery(editText.getText().toString()).build(MapsActivity.this);

                //Start activity result
                startActivityForResult(intent, 100);
                firstTime = false;
            }
        });
        // ======================================================================
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = mContext.getSharedPreferences("MyPref", mContext.MODE_PRIVATE);
        String latitude_d = prefs.getString("MyLatitude", null);
        String longitude_d = prefs.getString("MyLongitude", null);

        if (latitude_d != null) {
            Double l1 = Double.valueOf(latitude_d);
            Double l2 = Double.valueOf(longitude_d);
            currentLatLng = new LatLng(l1, l2);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100 && resultCode == RESULT_OK) {

            Place place = Autocomplete.getPlaceFromIntent(data);
            editText.setText(place.getAddress());

            LatLng destination = place.getLatLng();
            Double l1 = destination.latitude;
            Double l2 = destination.longitude;
            String latitude = l1.toString();
            String longitude = l2.toString();

            SharedPreferences pref = mContext.getSharedPreferences("MyPref", mContext.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("latitude", latitude);
            editor.putString("longitude", longitude);
            editor.apply();
            mapFragment.drawRoute(destination);


        } else if (requestCode == AutocompleteActivity.RESULT_ERROR) {
            //when error
            //initialize status
            Status status = Autocomplete.getStatusFromIntent(data);
            //display toast
            Toast.makeText(getApplicationContext(), status.getStatusMessage(),
                    Toast.LENGTH_SHORT).show();
        }
    }

}

