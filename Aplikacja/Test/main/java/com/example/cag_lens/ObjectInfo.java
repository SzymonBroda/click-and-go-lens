package com.example.cag_lens;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ObjectInfo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_object_info);
        int previousActivity = getIntent().getExtras().getInt("MAIN_ACTIVITY");
        String objectInfo;
        if(previousActivity == 1){
            objectInfo = getIntent().getExtras().getString("INFO");
        }
        else{
            objectInfo = getSharedPreferences("Preferences", Context.MODE_PRIVATE).getString("text", "");
        }

        setInfoOnTextView(objectInfo);
        startButtonClickListeners();
    }


    private void setInfoOnTextView(String text){
        TextView textView = findViewById(R.id.textView);
        textView.setTextSize(12);
        textView.setText(text);
    }

    private void startButtonClickListeners(){
        ImageButton btnBack;
        Button btnCustomize;
        btnBack = findViewById(R.id.backToCameraButton);
        btnCustomize = findViewById(R.id.customizeButton);
        btnBack.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ObjectInfo.this, MainActivity.class);
                        startActivity(intent);
                    }
                });

        btnCustomize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ObjectInfo.this, CustomizeObjects.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        TextView textView = (TextView) findViewById(R.id.textView);
        SharedPreferences.Editor prefEditor = getSharedPreferences("Preferences", Context.MODE_PRIVATE).edit();
        prefEditor.putString("text", textView.getText().toString());
        prefEditor.apply();
    }

}