package com.example.cag_lens;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.libraries.places.api.Places;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MinimapFragment extends Fragment implements OnMapReadyCallback {

    private MapView mapView;
    private GoogleMap mMap;
    LatLng currentLatLng;
    private Context mContext;
    private boolean firstTime = true;
    ArrayList<ArrayList<String>> navigation_hints = new ArrayList<ArrayList<String>>();
    ArrayList<LatLng> hints_points = new ArrayList<LatLng>();
    ArrayList<String> hints_list = new ArrayList<String>();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext=context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.maps_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        //Initialize places
        Places.initialize(mContext.getApplicationContext(), getString(R.string.google_maps_key));

        mapView = view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);
    }

    @Override
    public void onResume() {
        SharedPreferences prefs = mContext.getSharedPreferences("MyPref", mContext.MODE_PRIVATE);
        String latitude_d = prefs.getString("MyLatitude", null);
        String longitude_d = prefs.getString("MyLongitude", null);

        if (latitude_d != null) {
            Double l1 = Double.valueOf(latitude_d);
            Double l2 = Double.valueOf(longitude_d);
            currentLatLng = new LatLng(l1, l2);
        }

        super.onResume();
    }


    private void convertHints(ArrayList<ArrayList<String>> navigation_hints) {
        hints_points.clear();
        hints_list.clear();
        for (int i = 0; i<navigation_hints.size();i++)
        {
            double lat = Double.parseDouble(navigation_hints.get(i).get(0));
            double lng = Double.parseDouble(navigation_hints.get(i).get(1));
            LatLng point = new LatLng(lat, lng);
            hints_points.add(point);
            hints_list.add(navigation_hints.get(i).get(2));
        }

    }


    // ======================================================================



    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            // polaczenie http z url
            urlConnection = (HttpURLConnection) url.openConnection();
            // podlaczenie url
            urlConnection.connect();
            // odczyt danychz z url
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            Log.d("downloadUrl", data.toString());
            br.close();
        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;
            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask", jsonData[0].toString());
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());
// parsowanie
                routes = parser.parse(jObject);
                Log.d("ParserTask", "Executing routes");
                Log.d("ParserTask", routes.toString());

// uzyskanie odleglosci i czasu
                JSONArray jRoutes = jObject.getJSONArray("routes");
                if (jRoutes.length() != 0){
                    int time_in_sec = parser.calc_time_in_sec(jRoutes.getJSONObject(jRoutes.length()-1)); // ostatnia dostepna trasa
                    int dist_in_m = parser.calc_dist_in_m(jRoutes.getJSONObject(jRoutes.length()-1));
                    System.out.println("Total time: " + time_in_sec + " s, Total distance: " + dist_in_m + " m");
                    //System.out.println("Total time: " + time_in_sec/60 + " min, Total distance: " + dist_in_m/1000 + " km");
                }

                Log.d("ParserTask", parser.getAddr(jRoutes.getJSONObject(0)));
                String address = parser.getAddr(jRoutes.getJSONObject(0));
                SharedPreferences prefs = mContext.getApplicationContext().getSharedPreferences("MyPref", mContext.getApplicationContext().MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt("Destinations_size", 3);

                navigation_hints = parser.find_navigation_hints(jRoutes.getJSONObject(0));

                //Set<String> set = prefs.getStringSet("Destinations", null);
                String dests[] = new String[3];
                for(int i=0; i<3; i++) {
                    dests[i] = prefs.getString("Destinations" + "_" + i, null);
                }
                dests[2] = dests[1];
                dests[1] = dests[0];
                dests[0] = address;

                for(int i=0;i<dests.length;i++) {
                    editor.putString("Destinations" + "_" + i, dests[i]);
                }
                editor.apply();

            } catch (Exception e) {
                Log.d("ParserTask", e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;

            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                List<HashMap<String, String>> path = result.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);
                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);
                    points.add(position);
                }

                lineOptions.addAll(points);
                lineOptions.width(7);
                lineOptions.color(Color.RED);
                Log.d("onPostExecute", " onPostExecute decoded");
            }


            convertHints(navigation_hints);
            Log.d("Hints points: ", String.valueOf(hints_points));
            for (int i=0; i< hints_points.size(); i++){
                mMap.addMarker(new MarkerOptions().position(hints_points.get(i)).title("point"+i));
            }




// polyline na punktach
            if (lineOptions != null) {
                mMap.addPolyline(lineOptions);
            } else {
                Log.d("onPostExecute", "onPostExecute failed");
            }
        }
    }


    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();
            parserTask.execute(result);
        }
    }


    public void drawRoute(LatLng point) {
        mMap.clear();
        mMap.addMarker(new MarkerOptions().position(currentLatLng).title("You are here"));

        MarkerOptions destination = new MarkerOptions().position(point).title("Destination");
        mMap.addMarker(destination);
        System.out.println(point.latitude + "---" + point.longitude);
        String url = getDirectionsUrl(currentLatLng, point);


        FetchUrl FetchUrl = new FetchUrl();
        FetchUrl.execute(url);

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(currentLatLng);
        builder.include(point);

        int padding = 100;
        LatLngBounds bounds = builder.build();
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding));

    }


    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                SharedPreferences prefs = mContext.getApplicationContext().getSharedPreferences("MyPref", mContext.getApplicationContext().MODE_PRIVATE);
                String latitude_d = prefs.getString("myLatitude", null);
                String longitude_d = prefs.getString("myLongitude", null);
                if (latitude_d != null) {
                    Double l1 = Double.valueOf(latitude_d);
                    Double l2 = Double.valueOf(longitude_d);
                    currentLatLng = new LatLng(l1, l2);
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 16));
                    mMap.clear();
                    mMap.addMarker(new MarkerOptions().position(currentLatLng).title("You are here"));
                }

                latitude_d = prefs.getString("latitude", null);
                longitude_d = prefs.getString("longitude", null);
                if (latitude_d != null) {
                    Double l1 = Double.valueOf(latitude_d);
                    Double l2 = Double.valueOf(longitude_d);
                    LatLng point = new LatLng(l1, l2);

                    if (currentLatLng != null && mMap!=null )
                        drawRoute(point);
                }
            }
        });

        mMap.setOnMapClickListener(
                new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng point) {

                        Double l1 = point.latitude;
                        Double l2 = point.longitude;
                        String latitude = l1.toString();
                        String longitude = l2.toString();

                        SharedPreferences pref = mContext.getSharedPreferences("MyPref", mContext.MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("latitude", latitude);
                        editor.putString("longitude", longitude);
                        editor.apply();

                        drawRoute(point);
                    }
                });
    }


    private String getDirectionsUrl(LatLng origin, LatLng dest) {
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        String sensor = "sensor=false";

        String mode = "walking";
        String parameters = str_origin + "&" + str_dest + "&" + sensor;
        String output = "json";

        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&mode=" + mode + "&key=" + getString(R.string.google_maps_key);
        return url;
    }

}
