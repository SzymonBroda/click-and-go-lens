package com.example.cag_lens;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;

public class CustomizeObjects extends AppCompatActivity {


    CheckBox universityCheckBox, restaurantCheckBox, schoolCheckBox, nightclubCheckBox, pharmacyCheckBox;
    CheckBox parkCheckBox, libraryCheckBox, shoppingmallCheckBox, postofficeCheckBox, supermarketCheckBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customize_objects);
        universityCheckBox = findViewById(R.id.universityCheckBox);
        restaurantCheckBox = findViewById(R.id.restaurantCheckBox);
        schoolCheckBox = findViewById(R.id.schoolCheckBox);
        nightclubCheckBox = findViewById(R.id.nightclubCheckBox);
        pharmacyCheckBox = findViewById(R.id.pharmacyCheckBox);
        parkCheckBox = findViewById(R.id.parkCheckBox);
        libraryCheckBox = findViewById(R.id.libraryCheckBox);
        shoppingmallCheckBox = findViewById(R.id.shoppingmallCheckBox);
        postofficeCheckBox = findViewById(R.id.postOfficeCheckBox);
        supermarketCheckBox = findViewById(R.id.supermarketCheckBox);

        universityCheckBox.setChecked(getSharedPreferences("Preferences", Context.MODE_PRIVATE).getBoolean("university", false));
        restaurantCheckBox.setChecked(getSharedPreferences("Preferences", Context.MODE_PRIVATE).getBoolean("restaurant", false));
        schoolCheckBox.setChecked(getSharedPreferences("Preferences", Context.MODE_PRIVATE).getBoolean("school", false));
        nightclubCheckBox.setChecked(getSharedPreferences("Preferences", Context.MODE_PRIVATE).getBoolean("nightclub", false));
        pharmacyCheckBox.setChecked(getSharedPreferences("Preferences", Context.MODE_PRIVATE).getBoolean("pharmacy", false));
        parkCheckBox.setChecked(getSharedPreferences("Preferences", Context.MODE_PRIVATE).getBoolean("park", false));
        libraryCheckBox.setChecked(getSharedPreferences("Preferences", Context.MODE_PRIVATE).getBoolean("library", false));
        shoppingmallCheckBox.setChecked(getSharedPreferences("Preferences", Context.MODE_PRIVATE).getBoolean("shoppingmall", false));
        postofficeCheckBox.setChecked(getSharedPreferences("Preferences", Context.MODE_PRIVATE).getBoolean("postoffice", false));
        supermarketCheckBox.setChecked(getSharedPreferences("Preferences", Context.MODE_PRIVATE).getBoolean("supermarket", false));


        startButtonClickListeners();
    }

    private void startButtonClickListeners() {
        ImageButton btnBack;
        btnBack = findViewById(R.id.backToObjectButton);
        btnBack.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(CustomizeObjects.this, ObjectInfo.class);
                        intent.putExtra("MAIN_ACTIVITY", 0);
                        startActivity(intent);
                    }
                });

    }

    @Override
    public void onStop() {
        super.onStop();
        SharedPreferences.Editor prefEditor = getSharedPreferences("Preferences", Context.MODE_PRIVATE).edit();
        prefEditor.putBoolean("university", universityCheckBox.isChecked());
        prefEditor.putBoolean("school", schoolCheckBox.isChecked());
        prefEditor.putBoolean("postoffice", postofficeCheckBox.isChecked());
        prefEditor.putBoolean("shoppingmall", shoppingmallCheckBox.isChecked());
        prefEditor.putBoolean("supermarket", supermarketCheckBox.isChecked());
        prefEditor.putBoolean("library", libraryCheckBox.isChecked());
        prefEditor.putBoolean("park", parkCheckBox.isChecked());
        prefEditor.putBoolean("restaurant", universityCheckBox.isChecked());
        prefEditor.putBoolean("nightclub", nightclubCheckBox.isChecked());
        prefEditor.putBoolean("pharmacy", pharmacyCheckBox.isChecked());

        prefEditor.apply();
    }




}