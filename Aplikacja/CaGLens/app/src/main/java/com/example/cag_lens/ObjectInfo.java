package com.example.cag_lens;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ObjectInfo extends AppCompatActivity {

    Button btnCustomize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_object_info);
        int previousActivity = getIntent().getExtras().getInt("MAIN_ACTIVITY");
        String objectInfo;
        if(previousActivity == 1){
            objectInfo = getIntent().getExtras().getString("INFO");
        }
        else{
            objectInfo = getSharedPreferences("Preferences", Context.MODE_PRIVATE).getString("text", "");
        }
        btnCustomize = findViewById(R.id.customizeButton);
        btnCustomize.setVisibility(View.VISIBLE);
        setInfoOnTextView(objectInfo);
        startButtonClickListeners();
    }


    private void setInfoOnTextView(String text){
        TextView textView = findViewById(R.id.textView);
        textView.setTextSize(14);

        // Zwiększenie czcionki dla kategorii
        SpannableString spannableText = new SpannableString(text);
        setFontSizeForCategory(spannableText, "All categories:",(int)textView.getTextSize()+10);
        setFontSizeForCategory(spannableText, "Universities:",(int)textView.getTextSize()+10);
        setFontSizeForCategory(spannableText, "Restaurants:",(int)textView.getTextSize()+10);
        setFontSizeForCategory(spannableText, "Schools:",(int)textView.getTextSize()+10);
        setFontSizeForCategory(spannableText, "Night clubs:",(int)textView.getTextSize()+10);
        setFontSizeForCategory(spannableText, "Pharmacies:",(int)textView.getTextSize()+10);
        setFontSizeForCategory(spannableText, "Parks:",(int)textView.getTextSize()+10);
        setFontSizeForCategory(spannableText, "Libraries:",(int)textView.getTextSize()+10);
        setFontSizeForCategory(spannableText, "Shopping malls:",(int)textView.getTextSize()+10);
        setFontSizeForCategory(spannableText, "Post offices:",(int)textView.getTextSize()+10);
        setFontSizeForCategory(spannableText, "Supermarkets:",(int)textView.getTextSize()+10);

        textView.setText(spannableText);
    }

    private static void setFontSizeForCategory(Spannable spannable, String category, int fontSizeInPixel) {
        int startIndexOfCategory = spannable.toString().indexOf(category);
        if (startIndexOfCategory != -1)
            spannable.setSpan(new AbsoluteSizeSpan(fontSizeInPixel), startIndexOfCategory,
                    startIndexOfCategory + category.length(), 0);
    }

    private void startButtonClickListeners(){
        ImageButton btnBack;
        btnBack = findViewById(R.id.backToCameraButton);
        btnBack.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ObjectInfo.this, MainActivity.class);
                        startActivity(intent);
                    }
                });

        btnCustomize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setInfoOnTextView("");
                btnCustomize.setVisibility(View.INVISIBLE);
                Intent intent = new Intent(ObjectInfo.this, CustomizeObjects.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        TextView textView = (TextView) findViewById(R.id.textView);
        SharedPreferences.Editor prefEditor = getSharedPreferences("Preferences", Context.MODE_PRIVATE).edit();
        prefEditor.putString("text", textView.getText().toString());
        prefEditor.apply();

    }

}
