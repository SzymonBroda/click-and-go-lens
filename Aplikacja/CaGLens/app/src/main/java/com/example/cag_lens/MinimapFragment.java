package com.example.cag_lens;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MinimapFragment extends Fragment implements OnMapReadyCallback {

    private MapView mapView;
    private GoogleMap mMap;
    LatLng currentLatLng;
    private Context mContext;
    private static final int REQUEST_CODE = 101;
    Marker locationMarker;
    int time_in_sec, dist_in_m;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext=context;
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            // polaczenie http z url
            urlConnection = (HttpURLConnection) url.openConnection();
            // podlaczenie url
            urlConnection.connect();
            // odczyt danychz z url
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            Log.d("downloadUrl", data.toString());
            br.close();
        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            MinimapFragment.ParserTask parserTask = new MinimapFragment.ParserTask();
            parserTask.execute(result);
        }
    }


    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;
            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask", jsonData[0].toString());
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());
// parsowanie
                routes = parser.parse(jObject);
                Log.d("ParserTask", "Executing routes");
                Log.d("ParserTask", routes.toString());

                // uzyskanie odleglosci i czasu
                JSONArray jRoutes = jObject.getJSONArray("routes");
                if (jRoutes.length() != 0){
                    time_in_sec = parser.calc_time_in_sec(jRoutes.getJSONObject(jRoutes.length()-1)); // ostatnia dostepna trasa
                    dist_in_m = parser.calc_dist_in_m(jRoutes.getJSONObject(jRoutes.length()-1));
                    String timeToDestinationPoint = formatTime(time_in_sec);
                    String distanceToDestinationPoint = formatDistance(dist_in_m);
                    Log.d("Time to destination", timeToDestinationPoint);
                    Log.d("Distance to destination", distanceToDestinationPoint);
                }
            } catch (Exception e) {
                Log.d("ParserTask", e.toString());
                e.printStackTrace();
            }
            return routes;
        }


            @Override
            protected void onPostExecute(List<List<HashMap<String, String>>> result) {
                ArrayList<LatLng> points;
                PolylineOptions lineOptions = null;
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList<>();
                    lineOptions = new PolylineOptions();

                    List<HashMap<String, String>> path = result.get(i);

                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);
                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);
                        points.add(position);
                        builder.include(position);
                    }

                    lineOptions.addAll(points);
                    lineOptions.width(7);
                    lineOptions.color(Color.RED);
                    int padding = 100;
                    LatLngBounds bounds = builder.build();
                    mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding));
                    Log.d("onPostExecute", " onPostExecute decoded");
                }
// polyline na punktach
                if (lineOptions != null) {
                    mMap.addPolyline(lineOptions);
                } else {
                    Log.d("onPostExecute", "onPostExecute failed");
                }
            }
    }


    private class FetchUrl2 extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            MinimapFragment.ParserTask2 parserTask2 = new MinimapFragment.ParserTask2();
            parserTask2.execute(result);
        }
    }


    private class ParserTask2 extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;
            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask", jsonData[0].toString());
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());
// parsowanie
                routes = parser.parse(jObject);
                Log.d("ParserTask", "Executing routes");
                Log.d("ParserTask", routes.toString());

                // uzyskanie odleglosci i czasu
                JSONArray jRoutes = jObject.getJSONArray("routes");
                if (jRoutes.length() != 0){
                    time_in_sec = parser.calc_time_in_sec(jRoutes.getJSONObject(jRoutes.length()-1)); // ostatnia dostepna trasa
                    dist_in_m = parser.calc_dist_in_m(jRoutes.getJSONObject(jRoutes.length()-1));
                    String timeToDestinationPoint = formatTime(time_in_sec);
                    String distanceToDestinationPoint = formatDistance(dist_in_m);
                    Log.d("Time to destination2", timeToDestinationPoint);
                    Log.d("Dist to destination2", distanceToDestinationPoint);
                }
            } catch (Exception e) {
                Log.d("ParserTask", e.toString());
                e.printStackTrace();
            }
            return routes;
        }



    }


    // --- Metody odpowiedzalne za konwersję czasu i dystansu ---
    @SuppressLint("DefaultLocale")
    public String formatTime(int timeInSeconds){
        // Przetworzenie czasu z postaci sekundowej na format d h m s
        String formattedTime = "";
        int timeDays = timeInSeconds / 86400;
        int timeHours = (timeInSeconds - (timeDays*86400)) / 3600;
        int timeMinutes = (timeInSeconds - (timeHours*3600) - (timeDays*86400)) / 60;
        int timeSeconds = timeInSeconds - (timeMinutes*60) - (timeHours*3600) - (timeDays*86400);

        formattedTime =
                timeInSeconds >= 86400
                        ? String.format("%dd %dh %dm %ds", timeDays, timeHours, timeMinutes, timeSeconds)
                        : timeInSeconds >= 3600
                        ? String.format("%dh %dm %ds", timeHours, timeMinutes, timeSeconds)
                        : timeInSeconds >= 60
                        ?  String.format("%dm %ds", timeMinutes, timeSeconds)
                        :  String.format("%ds", timeSeconds);

        return formattedTime;
    }

    @SuppressLint("DefaultLocale")
    public String formatDistance(int distanceInMeters){
        // Przetworzenie dystansu w metrach na kilometry, jeśli to konieczne
        String formattedDistance = "";

        formattedDistance =
                distanceInMeters >= 1000
                        ? String.format("%.3f km", distanceInMeters/1000.0)
                        : String.format("%d m", distanceInMeters);

        return formattedDistance;
    }
    // ----------------------------------------------------------


    class PositionAsync extends AsyncTask<String, Void, LatLng> {
        @Override
        protected LatLng doInBackground(String... strings) {

            return null;
        }

        @Override
        protected void onPostExecute(LatLng point) {
            final Handler handler = new Handler();
            final SharedPreferences prefs = mContext.getSharedPreferences("MyPref", mContext.MODE_PRIVATE);

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    handler.postDelayed(this, 4010);
                    String latitude_d = prefs.getString("myLatitude", null);
                    String longitude_d = prefs.getString("myLongitude", null);
                    if (latitude_d != null) {
                        Double l1 = Double.valueOf(latitude_d);
                        Double l2 = Double.valueOf(longitude_d);
                        currentLatLng = new LatLng(l1, l2);
                        locationMarker.remove();
                        locationMarker = mMap.addMarker(new MarkerOptions().position(currentLatLng).title("You are here").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
                    }

                    // Zapytanie Directions Api w celu znalezienia drogi i czasu do celu
                    latitude_d = prefs.getString("latitude", null);
                    longitude_d = prefs.getString("longitude", null);
                    if (latitude_d != null) {
                        Double l1 = Double.valueOf(latitude_d);
                        Double l2 = Double.valueOf(longitude_d);
                        LatLng point1 = new LatLng(l1, l2);
                        String url = getDirectionsUrl(currentLatLng, point1);
                        FetchUrl2 FetchUrl2 = new FetchUrl2();
                        FetchUrl2.execute(url);
                    }
                }
            }, 10);

        }
    }
    public void drawRoute(LatLng point){
        mMap.clear();
        locationMarker = mMap.addMarker(new MarkerOptions().position(currentLatLng).title("You are here").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));

        MarkerOptions destination = new MarkerOptions().position(point).title("Destination");
        mMap.addMarker(destination);
        System.out.println(point.latitude + "---" + point.longitude);
        String url = getDirectionsUrl(currentLatLng, point);


        FetchUrl FetchUrl = new FetchUrl();
        FetchUrl.execute(url);
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(currentLatLng);
        builder.include(point);

        int padding = 100;
        LatLngBounds bounds = builder.build();
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding));
        PositionAsync positionUpdate = new PositionAsync();
        positionUpdate.execute();
        }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        String sensor = "sensor=false";

        String mode = "walking";
        String parameters = str_origin + "&" + str_dest + "&" + sensor;
        String output = "json";

        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&mode=" + mode + "&key=" + getString(R.string.google_maps_key);

        return url;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.maps_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        mapView = view.findViewById(R.id.mapView2);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);
    }


    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                SharedPreferences prefs = mContext.getSharedPreferences("MyPref", mContext.MODE_PRIVATE);
                String latitude_d = prefs.getString("myLatitude", null);
                String longitude_d = prefs.getString("myLongitude", null);
                if (latitude_d != null) {
                    Double l1 = Double.valueOf(latitude_d);
                    Double l2 = Double.valueOf(longitude_d);
                    currentLatLng = new LatLng(l1, l2);
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 16));
                    mMap.clear();
                    locationMarker = mMap.addMarker(new MarkerOptions().position(currentLatLng).title("You are here").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));

                }

                latitude_d = prefs.getString("latitude", null);
                longitude_d = prefs.getString("longitude", null);
                if (latitude_d != null) {
                    Double l1 = Double.valueOf(latitude_d);
                    Double l2 = Double.valueOf(longitude_d);
                    LatLng point = new LatLng(l1, l2);

                    if (currentLatLng != null && mMap!=null )
                        drawRoute(point);
                }
            }
        });

    }

    }
