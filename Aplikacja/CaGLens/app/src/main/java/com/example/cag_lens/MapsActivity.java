package com.example.cag_lens;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    private ImageButton btnBack;
    AutoCompleteTextView editText;
    private GoogleMap mMap;
    private Context mContext;
    LatLng currentLatLng;
    private boolean firstTime = true;
    ArrayList<ArrayList<String>> navigation_hints;
    ArrayList<LatLng> hints_points = new ArrayList();
    ArrayList<String> hints_list = new ArrayList();


    private static final int REQUEST_CODE = 101;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(MapsActivity.this, MainActivity.class);
        intent.putExtra("navigation_hints",navigation_hints);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapView);
        mapFragment.getMapAsync(this);


        mContext = getApplicationContext();
        btnBack = findViewById(R.id.mapsBackButton);

        btnBack.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MapsActivity.this, MainActivity.class);
                        intent.putExtra("navigation_hints",navigation_hints);
                        startActivity(intent);
                    }
                });

        // ======================================================================
        //Assign variable
        editText = (AutoCompleteTextView) findViewById(R.id.edit_text);

        //Initialize places
        Places.initialize(getApplicationContext(), getString(R.string.google_maps_key));

        //Set EditText non Focusable
        editText.setFocusable(false);

        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Place.Field> fieldList = Arrays.asList(Place.Field.ADDRESS,
                        Place.Field.LAT_LNG, Place.Field.NAME);
                //Create intent

                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY,
                        fieldList).setInitialQuery(editText.getText().toString()).build(MapsActivity.this);

                //Start activity result
                startActivityForResult(intent, 100);
                firstTime = false;
            }
        });

        String message = null;
        Intent intent_dest = getIntent();
        String destination_str = intent_dest.getStringExtra("destination_str");
        if (destination_str!=null) {
            List<Place.Field> fieldList = Arrays.asList(Place.Field.ADDRESS,
                    Place.Field.LAT_LNG, Place.Field.NAME);
            //Create intent

            Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY,
                    fieldList).setInitialQuery(destination_str).build(this);
            startActivityForResult(intent, 100);
        }
        else{
            try {
                Bundle bundle = getIntent().getExtras();
                message = bundle.getString("destination");
            } catch (Exception ignored) {}

            if(message != null){
                List<Place.Field> fieldList = Arrays.asList(Place.Field.ADDRESS,
                        Place.Field.LAT_LNG, Place.Field.NAME);

                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY,
                        fieldList).setInitialQuery(message).build(MapsActivity.this);

                startActivityForResult(intent, 100);
                firstTime = false;
            }
        }
        // ======================================================================
    }


    // ======================================================================

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100 && resultCode == RESULT_OK) {

            Place place = Autocomplete.getPlaceFromIntent(data);
            editText.setText(place.getAddress());

            LatLng destination = place.getLatLng();
            Double l1 = destination.latitude;
            Double l2 = destination.longitude;
            String latitude = l1.toString();
            String longitude = l2.toString();

            SharedPreferences pref = mContext.getSharedPreferences("MyPref", mContext.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("latitude", latitude);
            editor.putString("longitude", longitude);
            editor.apply();
            drawRoute(destination,mMap);


        } else if (requestCode == AutocompleteActivity.RESULT_ERROR) {
            //when error
            //initialize status
            Status status = Autocomplete.getStatusFromIntent(data);
            //display toast
            Toast.makeText(getApplicationContext(), status.getStatusMessage(),
                    Toast.LENGTH_SHORT).show();
        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            // polaczenie http z url
            urlConnection = (HttpURLConnection) url.openConnection();
            // podlaczenie url
            urlConnection.connect();
            // odczyt danychz z url
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            Log.d("downloadUrl", data.toString());
            br.close();
        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;
            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask", jsonData[0].toString());
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());
// parsowanie
                routes = parser.parse(jObject);
                Log.d("ParserTask", "Executing routes");
                Log.d("ParserTask", routes.toString());

// uzyskanie odleglosci i czasu
                JSONArray jRoutes = jObject.getJSONArray("routes");


                Log.d("ParserTask", parser.getAddr(jRoutes.getJSONObject(0)));
                String address = parser.getAddr(jRoutes.getJSONObject(0));
                SharedPreferences prefs = getApplicationContext().getSharedPreferences("MyPref", getApplicationContext().MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt("Destinations_size", 5);

                navigation_hints = parser.find_navigation_hints(jRoutes.getJSONObject(0));

                Boolean alreadyThere = false;
                String dests[] = new String[5];
                for(int i=0; i<5; i++) {
                    dests[i] = prefs.getString("Destinations" + "_" + i, null);
                    System.out.println(dests[i]);
                    System.out.println("=======================================================================");
                    if(dests[i] != null && dests[i].equals(address))
                        alreadyThere = true;
                }
                if(!alreadyThere) {
                    dests[4] = dests[3];
                    dests[3] = dests[2];
                    dests[2] = dests[1];
                    dests[1] = dests[0];
                    dests[0] = address;

                    for(int i=0;i<dests.length;i++) {
                        editor.putString("Destinations" + "_" + i, dests[i]);
                    }
                    editor.apply();
                }

            } catch (Exception e) {
                Log.d("ParserTask", e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                List<HashMap<String, String>> path = result.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);
                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);
                    points.add(position);
                    builder.include(position);
                }

                lineOptions.addAll(points);
                lineOptions.width(7);
                lineOptions.color(Color.RED);
                int padding = 250;
                LatLngBounds bounds = builder.build();
                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding));
                Log.d("onPostExecute", " onPostExecute decoded");
            }



// polyline na punktach
            if (lineOptions != null) {
                mMap.addPolyline(lineOptions);
            } else {
                Log.d("onPostExecute", "onPostExecute failed");
            }
        }
    }


    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            MapsActivity.ParserTask parserTask = new MapsActivity.ParserTask();
            parserTask.execute(result);
        }
    }


    public void drawRoute(LatLng point, GoogleMap mMap) {
        mMap.clear();
        if (currentLatLng!=null && point !=null) {
            mMap.addMarker(new MarkerOptions().position(currentLatLng).title("You are here").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));

            MarkerOptions destination = new MarkerOptions().position(point).title("Destination");
            mMap.addMarker(destination);
            System.out.println(point.latitude + "---" + point.longitude);
            String url = getDirectionsUrl(currentLatLng, point);


            MapsActivity.FetchUrl FetchUrl = new MapsActivity.FetchUrl();
            FetchUrl.execute(url);

        }
    }


    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                SharedPreferences prefs = getApplicationContext().getSharedPreferences("MyPref", getApplicationContext().MODE_PRIVATE);
                String latitude_d = prefs.getString("myLatitude", null);
                String longitude_d = prefs.getString("myLongitude", null);
                if (latitude_d != null) {
                    Double l1 = Double.valueOf(latitude_d);
                    Double l2 = Double.valueOf(longitude_d);
                    currentLatLng = new LatLng(l1, l2);
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 16));
                    mMap.clear();
                    mMap.addMarker(new MarkerOptions().position(currentLatLng).title("You are here").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
                }

                latitude_d = prefs.getString("latitude", null);
                longitude_d = prefs.getString("longitude", null);
                if (latitude_d != null) {
                    Double l1 = Double.valueOf(latitude_d);
                    Double l2 = Double.valueOf(longitude_d);
                    LatLng point = new LatLng(l1, l2);

                    if (currentLatLng != null && mMap!=null )
                        drawRoute(point,mMap);
                }
            }
        });

        mMap.setOnMapClickListener(
                new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng point) {

                        Double l1 = point.latitude;
                        Double l2 = point.longitude;
                        String latitude = l1.toString();
                        String longitude = l2.toString();

                        SharedPreferences pref = mContext.getSharedPreferences("MyPref", mContext.MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("latitude", latitude);
                        editor.putString("longitude", longitude);
                        editor.apply();

                        drawRoute(point, mMap);
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = mContext.getSharedPreferences("MyPref", mContext.MODE_PRIVATE);
        String latitude_d = prefs.getString("MyLatitude", null);
        String longitude_d = prefs.getString("MyLongitude", null);

        if (latitude_d != null) {
            Double l1 = Double.valueOf(latitude_d);
            Double l2 = Double.valueOf(longitude_d);
            currentLatLng = new LatLng(l1, l2);
        }
    }


    private String getDirectionsUrl(LatLng origin, LatLng dest) {
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        String sensor = "sensor=false";

        String mode = "walking";
        String parameters = str_origin + "&" + str_dest + "&" + sensor;
        String output = "json";
        String language ="pl";

        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&mode=" + mode + "&key=" + getString(R.string.google_maps_key)+"&language="+language;
        return url;
    }


}

