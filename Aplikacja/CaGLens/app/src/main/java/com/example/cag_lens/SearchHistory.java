package com.example.cag_lens;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;

public class SearchHistory extends AppCompatActivity {

    private Context mContext;
    Button buttons[] = new Button[5];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_history);

        buttons[0] = findViewById(R.id.dest1);
        buttons[1] = findViewById(R.id.dest2);
        buttons[2] = findViewById(R.id.dest3);
        buttons[3] = findViewById(R.id.dest4);
        buttons[4] = findViewById(R.id.dest5);

        SharedPreferences prefs = getApplicationContext().getSharedPreferences("MyPref", getApplicationContext().MODE_PRIVATE);
        for(int i=0; i<5; i++) {
            buttons[i].setVisibility(View.INVISIBLE);
            String dest = prefs.getString("Destinations" + "_" + i, null);

            if(dest != null){
                buttons[i].setText(dest);
                buttons[i].setVisibility(View.VISIBLE);
            }
        }

        startButtonClickListeners();
    }

    private void startButtonClickListeners() {
        ImageButton btnBack;
        btnBack = findViewById(R.id.backToObjectButton2);

        btnBack.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(SearchHistory.this, MainActivity.class);
                        intent.putExtra("MAIN_ACTIVITY", 0);
                        startActivity(intent);
                    }
                });

        buttons[0].setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(SearchHistory.this, MapsActivity.class);
                        intent.putExtra("destination", buttons[0].getText());
                        startActivity(intent);
                    }
                });

        buttons[1].setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(SearchHistory.this, MapsActivity.class);
                        intent.putExtra("destination", buttons[1].getText());
                        startActivity(intent);
                    }
                });

        buttons[2].setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(SearchHistory.this, MapsActivity.class);
                        intent.putExtra("destination", buttons[2].getText());
                        startActivity(intent);
                    }
                });

        buttons[3].setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(SearchHistory.this, MapsActivity.class);
                        intent.putExtra("destination", buttons[3].getText());
                        startActivity(intent);
                    }
                });

        buttons[4].setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(SearchHistory.this, MapsActivity.class);
                        intent.putExtra("destination", buttons[4].getText());
                        startActivity(intent);
                    }
                });
    }

    @Override
    public void onStop() {
        super.onStop();
//        SharedPreferences.Editor prefEditor = getSharedPreferences("Preferences", Context.MODE_PRIVATE).edit();
//        prefEditor.putBoolean("university", universityCheckBox.isChecked());
//
//
//        prefEditor.apply();
    }




}
