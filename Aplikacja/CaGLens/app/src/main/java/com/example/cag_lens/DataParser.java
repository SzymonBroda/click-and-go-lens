package com.example.cag_lens;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataParser {
    List<List<HashMap<String,String>>> parse(JSONObject jObject){
        List<List<HashMap<String, String>>> routes = new ArrayList<>() ;
        JSONArray jRoutes;
        JSONArray jLegs;
        JSONArray jSteps;
        try {
            jRoutes = jObject.getJSONArray("routes");
            for(int i=0;i<jRoutes.length();i++){
                jLegs = ( (JSONObject)jRoutes.get(i)).getJSONArray("legs");
                List path = new ArrayList<>();
                for(int j=0;j<jLegs.length();j++){
                    jSteps = ( (JSONObject)jLegs.get(j)).getJSONArray("steps");
                    for(int k=0;k<jSteps.length();k++){
                        String polyline = "";
                        polyline = (String)((JSONObject)((JSONObject)jSteps.get(k)).get("polyline")).get("points");
                        List<LatLng> list = decodePoly(polyline);
                        for(int l=0;l<list.size();l++){
                            HashMap<String, String> hm = new HashMap<>();
                            hm.put("lat", Double.toString((list.get(l)).latitude) );
                            hm.put("lng", Double.toString((list.get(l)).longitude) );
                            path.add(hm);
                        }
                    }
                    routes.add(path);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){
        }
        return routes;
    }
    int calc_time_in_sec(JSONObject jObject){
        // Oblicza czas do celu w sekundach, dla trasy podanej jako parametr
        JSONArray jLegs;
        int time_in_sec = 0;
        try {
            jLegs = jObject.getJSONArray("legs");
            for(int j=0;j<jLegs.length();j++){
                time_in_sec += jLegs.getJSONObject(j).getJSONObject("duration").getInt("value");
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){
        }
        return time_in_sec;
    }

    int calc_dist_in_m(JSONObject jObject){
        // Oblicza dystans do celu w metrach, dla trasy podanej jako parametr
        JSONArray jLegs;
        int dist_in_m = 0;
        try {
            jLegs = jObject.getJSONArray("legs");
            for(int j=0;j<jLegs.length();j++){
                dist_in_m += jLegs.getJSONObject(j).getJSONObject("distance").getInt("value");
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){
        }
        return dist_in_m;
    }

    ArrayList<ArrayList<String>> find_navigation_hints(JSONObject jObject){
        // Zwraca liste wskazowek dotyczacych nawigacji razem z ich pozycjami w postaci (Lat, Lng, Wskazowka)
        // Na wejciu bierze wybrana trase
        ArrayList<ArrayList<String>> coor_hints = new ArrayList<>();
        JSONArray jLegs;
        JSONArray jSteps;

        try {
            jLegs = jObject.getJSONArray("legs");
            for(int j = 0; j < jLegs.length(); j++){
                // Iterowanie po krokach - najmniejszcyh odcinkach, na ktore podzielona jest trasa
                jSteps = jLegs.getJSONObject(j).getJSONArray("steps");
                for(int k = 0; k < jSteps.length(); k++){

                    ArrayList lat_lng_hints = new ArrayList<>();
                    lat_lng_hints.add(jSteps.getJSONObject(k).getJSONObject("start_location").getString("lat"));
                    lat_lng_hints.add(jSteps.getJSONObject(k).getJSONObject("start_location").getString("lng"));
                    String hint = jSteps.getJSONObject(k).getString("html_instructions");
                    hint = hint.replaceAll("<.*?>", " "); // usuniecie znacznikow HTML
                    hint = hint.replaceAll("&nbsp;mi", ""); // usuniecie nielamliwej spacji
                    hint = hint.replaceAll("  ", " "); // usuniecie podwojnych spacji
                    lat_lng_hints.add(hint);
                    coor_hints.add(lat_lng_hints);
                }
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){
        }
        return coor_hints;
    }
    /**
     * Method to decode polyline points
     * */
    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;
        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;
            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;
            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }
        return poly;
    }

    String getAddr(JSONObject jObject){
        // Zwraca adres docelowy
        String address = "";
        JSONArray jLegs;
        try {
            jLegs = jObject.getJSONArray("legs");
            address = jLegs.getJSONObject(jLegs.length()-1).getString("end_address");
        }
        catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){
        }
        return address;
    }
}
