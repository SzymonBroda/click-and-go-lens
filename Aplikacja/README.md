# Aplikacja CaG-Lens na telefon z systemem Android
W celu dokonywania zmian w projekcie należy ściagnąć folder "CaGLens" i otworzyć go w programie Android Studio.

Pliki z kodem źródłowym znajdują się w folderze "CaGLens/app/src/main".

Aby można było używać Google API na telefonie bądź emulatorze to musi ono posiadać Serwisy Google Play. W celu dodania tych serwisów do Android Studio należy wejść w "Settings/Appearance and Behaviour/System Settings/Android SDK/SDK Tools" i zaznaczyć na liście Google Play Services.

W celu użycia Google API w aplikacji należy również wygenerować swój klucz interfejsu API. Zrobić to można podążając za tym tutorialem:
https://developers.google.com/maps/documentation/android-sdk/get-api-key
Nastepnie, wygenerowany klucz API należy podmienić pod wartość "YOUR_API_KEY" w pliku "CaGLens/app/src/main/res/values/google_maps_api.xml".

Aby móc odczytywać obecną lokalizację należy do ograniczeń klucza dodać Geolocation API jak opisapo pod linkiem:
https://developers.google.com/maps/documentation/geolocation/get-api-key?hl=pl

Aby móc wysłać zapytanie dotyczące obiektów znajdujących się w pobliżu obecnej lokalizacji należy do ograniczeń klucza dodać Places API.

Aby móc odczytywać obecną lokalizację należy do ograniczeń klucza dodać Directions API oraz Places SDK for Android

