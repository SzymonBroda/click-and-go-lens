# Click and Go Lens dokumentacja
#
## Spis plików
###### Aplikacja:
- CameraFragment - fragment odpowiedzialny za wyświetlanie kamery przy użyciu klasy CameraView
- CameraView.java - klasa odpowiedzialna za obsługę kamery
- Compass.java - klasa odpowiedzialna z obsługę kompasu
- CustomizeObjects.java - klasa odpowiedzialna za wybór typów miejsc o których chcemy uzyskiwać infomacje
- DataParser.java - klasa zawierająca metody przetwarzające dane uzyskane przy pomocy Directions API
- MainActivity.java - główna klasa aktywności aplikacji
- MapsActivity.java - klasa aktywności zawierająca mapę i pole umożliwiające wyszukanie celu trasy
- MinimapFragment.java - fragment odpowiedzialny za wyświetlanie i obsługę minimapy w MainActivity
- ObjectInfo.java - klasa odpowiedzialna za wyświetlanie infomacji o najbliższych obiektach
- SearchHistory.java - klasa odpowiadająca za wyświetlanie oraz obsługę historii wyszukiwania
- SettingsActivity.java - Klasa która odpowiada za menu ustawiń

#
## Przyjęte rozwiązania



#### Nawigacja głosowa
W aplikacji wykorzystywany jest klasa TextToSpeech, udostępniana przez Google, który pozwala na odczytywania napisanego tekstu. Funkcjonalność ta umożliwia stworzenie nawigacji głosowej, która przekazuje odpowiednie polecenia dla użytkownika. Do poprawnego działania nawigacji głosowej wymagane są usługi Google Play.

Inicjalizacja TextToSpeech:
```sh
textToSpeech = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
    public void onInit(int status) {
    }
});
```

Odczytywanie tekstu z pomocą TextToSpeech:
```sh
textToSpeech.speak(tekst_do_odczytania, TextToSpeech.QUEUE_FLUSH, null);
```

#### Czas, prędkość i  odległość do celu

Długość trasy oraz czas potrzebny do jej przebycia są wyznaczane z odpowiedzi uzyskanej przy pomocy Directions API. Dla wybranej trasy sumowane są odległości (czasy przebycia) poszczególnych odcinków (ang. legs).

Wyznaczanie długości trasy:
```sh
jLegs = jObject.getJSONArray("legs");
for(int j=0;j<jLegs.length();j++){
	dist_in_m += jLegs.getJSONObject(j).getJSONObject("distance").getInt("value");
}
```

Obliczanie aktualnej prędkośći:
```sh
speed = Math.round(location.getSpeed() * 100.0) / 100.0 ;
```

Aktualizacji położenia odbywa się co 2 sekundy.
Funkcja pochodzi z pakietu android.location.
#### Wyświetlanie czasu, odległości do celu i prędkości

Uzyskanie prędkośći odbywa się w klasie *MainActivity.java* i metodzie *getSpeed()* klasy Location, która jest następnie formatowana do dwóch miejsc po przecinku. Czas do celu jest uzyskiwany za pomocą metody *formatTime()* klasy *MinimapFragment*, a dystans do celu metodą *formatDistance()* należącą do tej samej klasy.
```java
String time = minimapFragment.formatTime(minimapFragment.time_in_sec);
String dist=minimapFragment.formatDistance(minimapFragment.dist_in_m);
speed = Math.round(location.getSpeed() * 100.0) / 100.0 ;
textViewInfo.setText("Speed: "+speed+"m/s"+ "\nDist: "+dist+"\nTime: "+time);
```

#### Wyznaczanie wskazówek 
Wskazówki dotyczące nawigacji są wyznaczane z odpowiedzi uzyskanej przy pomocy Directions API. Wybrana trasa jest dzielona na dłuższe odcinki (legs), które składają się z krótszych odcinków (steps). Każdemu z krótszych odcinków odpowiada wskazówka w postaci tekstu HTML.  Po usunięciu znaczników oraz niełamliwych spacji jest ona zapisywana razem z lokalizacją, w której ma być odczytana.

Zapis wskazówki:
```sh
lat_lng_hints.add(jSteps.getJSONObject(k).getJSONObject("start_location").getString("lat"));
lat_lng_hints.add(jSteps.getJSONObject(k).getJSONObject("start_location").getString("lng"));
String hint = jSteps.getJSONObject(k).getString("html_instructions");
hint = hint.replaceAll("<.*?>", " "); // usuniecie znacznikow HTML
hint = hint.replaceAll("&nbsp;mi", ""); // usuniecie nielamliwej spacji
hint = hint.replaceAll("  ", " "); // usuniecie podwojnych spacji
lat_lng_hints.add(hint);
```

Wyznaczone wskazówki za pomocą klasy Intent przesyłane są z klasy MapsActivity do klasy MainActivity, gdzie rozdzielane są na dwie listy - jedna zawierająca wszystkie punkty trasy w postaci klasy LatLng, druga zawierająca wskazówki w postaci tekstowej. Listy te następnie wykorzystywane są do informowania użytkownika o zbliżających się wskazówkach.

#### Strzałka wskazująca drogę

W prawym dolnym rogu ekranu, nad minimapą, po wybraniu przez użytkownika trasy do celu wyświetlana jest niebieska strzałka, która wskazuje kierunek do najbliższego punktu na trasie. Kąt obrotu strzałki obliczany jest na podstawie dwóch punktów w postaci klasy LatLng - jeden to nasza obecna pozycja, a drugi to kolejny punkt trasy, którą się poruszamy. W kącie obrotu strzałki uwzględniony jest również kąt obrotu kompasu, dzięki czemu strzałka wskazuje rzeczywisty kierunek, w który powinniśmy się obroćić aby podążać trasą.

#### Dostęp do bieżacej lokalizacji
Aby umożliwić i aktualizować bieżącą lokalizację jest ona uzyskiwana za pośrednictwe metody *requestLocationUpdates()* klasy *FusedLocationProviderClient*. 
```java
fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
```
Następnie na jej podstawie wyznaczane są takie parametry jak długość i szerokość geograficzna.

```java
@Override
 public void onLocationResult(LocationResult locationResult) {
	 if (locationResult == null) {
		 return;
	 }
	 for (Location location : locationResult.getLocations()) {
		 Log.d("LOCATION", location.toString());
		 latidude = location.getLatitude();
		 longitude = location.getLongitude();
		 }
	 }
 };
```
#### Wyświetlanie miejsc w pobliżu
W celu otrzymania miejsc w pobliżu napisano klasę *getNearbyAsyncTask* jako zdarzenie asynchroniczne dziedzicząc po klasie *AsyncTask*.

Miejsca są wyznaczane na podstawie Places API. Poniżej znajduje się konstukcja adresu URL wykorzystywanego do wykonania żądania HTTP - nearbysearch. 
```java
sb = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?" +
  "location=" + String.valueOf(lat) + "," + String.valueOf(lng) +
  "&type=" + type +
  "&radius=" + String.valueOf(radius) +
  "&key=" + getString(R.string.google_maps_key);
```
Wynik zapytania przechowywany jest w formacie JSON (ang. *JavaScript Object Notation*). Do jego przetworzenia wykorzystano klasy *JSONObject, JSONArray*. Przetwarzanie zaprezentowano w skrócony sposób poniżej.
```java
try {
	JSONObject jsonObj = new JSONObject(jsonResults.toString());
	JSONArray predsJsonArray = jsonObj.getJSONArray("results");
	// Wydobycie informacji o miejscach
	for (int i = 0; i < predsJsonArray.length(); i++) {
		String name = predsJsonArray.getJSONObject(i).getString("name");
		String types = predsJsonArray.getJSONObject(i).getString("types");
		// ---
		// Wydobycie innych informacji np. o lokalizacji obiektu
		// Przetwarzanie powyzszych informacji w celu wywietlenia ich w przygotowanym widoku.
		// oraz utworzenie listy wykorzystywanej później w wyświetlaniu markera miejsc
		// ---
	}
} catch (JSONException e) {
	Log.e("Places", "Error processing JSON results", e);
}
```
W celu uzyskania informacji o miejscach w promieniu 300 metrów od aktualnej lokalizacji w metodzie *onLocationResult* wykonywane jest polecenie
```java
new getNearbyAsyncTask(myLatitude, myLongitude, 300, type).execute();
```

Wyświetlanie miejsc w pobliżu zachodzi w aktywności *ObjectsInfo.java* dzięki przesłaniu informacji o najbliższych miejsach z klasy *MainActivity.java* przy pomocy metody *putIntent()* klasy *Intent*.

```java
Intent intent = new Intent(MainActivity.this, ObjectInfo.class);
intent.putExtra("INFO", nearbyPlaces);
startActivity(intent);
```
Następnie informacja o obiektach jest odbierana i wyświetlana we właściwej aktywności w metodzie *onCreate()*. Jednocześnie jest sprawdzane, czy należy wyświetlić stary komunikat, jeśli przechodzimy z klasy odpowiadającej za konfiguracje preferowanych miejsc, czy z klasy *MainActivity.java* w której najbliższe miejsca są aktualizowane.
```java
 int previousActivity = getIntent().getExtras().getInt("MAIN_ACTIVITY");
 String objectInfo;
 if(previousActivity == 1){
	 objectInfo = getIntent().getExtras().getString("INFO");
 }
 else{
	 objectInfo = getSharedPreferences("Preferences", Context.MODE_PRIVATE).getString("text", "");
 }
setInfoOnTextView(objectInfo);
```
####Wybór preferowanych typów miejsc####
Aby umożliwić użytkownikowi wybór pozyskiwania informacji o preferowanych miejscach, stworzono aktywność z checkboxami których stany są zapamiętywane przy pomocy klasy *SharedPreferences.Edior* i jej metody *putBoolean()* w metodzie *onStop()* klasy *CustomizeObjects.java*  .
 ```java
 SharedPreferences.Editor prefEditor = getSharedPreferences("Preferences", Context.MODE_PRIVATE).edit();
 prefEditor.putBoolean("university", universityCheckBox.isChecked());
 prefEditor.apply();
 ```

Po ponownym wejściu w opcje, użytownik zobaczy swoje wcześniej ustalone preferencje dzięki metodzie *getBoolean()* klasy *SharedPreferences.Editor*  i zaznaczeniu odpowiedich checkboxów metodą *setChecked()* klasy *CheckBox* w metodzie *onCreate* klasy *CustomizeObjects.java*.
```java
universityCheckBox.setChecked(getSharedPreferences("Preferences", Context.MODE_PRIVATE).getBoolean("university", false));
```
#### Wyświetlanie markera miejsc
Jeśli włączona jest opcja wyszukiwania pobliskich miejsc zainteresowań, na obrazie z kamery może być wyświetlany marker wskazujący jeden z obiektów.  Marker pojawia się jeśli telefon jest obrócony w stronę miejsca zainteresowania (z określoną tolerancją) oraz miejsce to znajduje się dostatecznie blisko. Obok wyświetlana jest nazwa miejsca.

Wyświetlanie markera:
```sh
if(placeDistance < distanceThreshold){ // czy dostatecznie blisko
	double placeBearing = angle(myLocation, placeLocation);
	double diffAngle = Math.abs(placeBearing-compassActivity.getDegrees());
	diffAngle = diffAngle > 180.0 ? 360.0-diffAngle : diffAngle; // od 0 do 180
	if(diffAngle < angleThreshold){ // czy dobry kierunek
		setMarkerVisible = true;
		if(placeDistance < nearest_distance){
			nearest_place_name_text.setText(nearbyPlacesLocalization.get(i).name);
			// Ustawianie wielkosci markera
		}
	}
}
```
Rozmiar markera jest liniowo zależny od odległości od obecnej lokalizacji: 
```sh
placeMarkerView.getLayoutParams().height = dpToPx((int) (max_marker_height - (max_marker_height-min_marker_height)*placeDistance/distanceThreshold));
placeMarkerView.getLayoutParams().width = dpToPx((int)  (max_marker_width - (max_marker_width-min_marker_width)*placeDistance/distanceThreshold));
```
W przypadku nakładania się na siebie kilku miejsc wyświetlana jest nazwa najbliższego z nich.

#### Wyświetlanie minimapy
Jeśli opcja minimapy jest włączona to pojawi się ona na dole ekranu. Wszystkie elementy widoczne na dużej mapie także będą tam widoczne, włącznie z trasą i obecną pozycją. Minimapę można schować przyciskiem umieszczonym na samym dole ekranu. Wraz z poruszaniem się mapa będzie się przesuwać do naszej aktualnej pozycji:

Wyświetalnie obecnej pozycji:
```sh
currentLatLng = new LatLng(l1, l2);
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 16));
                    mMap.clear();
                    locationMarker = mMap.addMarker(new MarkerOptions().position(currentLatLng).title("You are here").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
```
Pozycja na minimapie jest aktualizowana razem z pozycją wyznaczoną w MainActivity. Przy każdyej aktualizacji lokalizacji jest ona zapisywana, następnie odzytwana w klasie PositionAsync.


#### Orientacja telefonu oraz kompas
Orientacja telefonu (a właściwie jedynie azymut) wyznaczana jest przy pomocy sensora typu rotation vector, jeśli jest on dostępny w danym urządzeniu. Jeśli urządzenie jest starszego typu, to wyznaczamy ją przy pomocy fuzji danych z akcelerometru i magnetometru. Aby kompas działał poprawnie, należy upewnić się, że w pobliżu nie znajdują się sztuczne źródła pola magnetycznego takie jak np. komputer lub inne domowe urządzenia.

Wyznaczenie azymutu przy pomocy czujnika rotation vector:
```java
        if(event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
            SensorManager.getRotationMatrixFromVector(rMat, event.values);
            SensorManager.remapCoordinateSystem(rMat, SensorManager.AXIS_X, SensorManager.AXIS_Z, rMatRemapped);
            mAzimuth = (int) (Math.toDegrees(SensorManager.getOrientation(rMatRemapped, orientation)[0]))%360;
        }
```

Kompas w postaci liniowej znajdujący się na górze ekranu to właściwie statyczna tekstura tła oraz kilka poruszających się w zależności od wyznaczonego azymutu tekstur: kierunek geograficzny (np. NW), dwa znaki '-' oraz znacznik określający cel nawigacji.

Przykład przesunięcia tekstury kierunku geograficznego NW:
```java
        if (degrees < 337 && degrees > 292) {
            directionImage.setImageResource(R.drawable.northwest);
            translation = (degrees-315)*trMultiplier;
        }
```
Aby kompas działał dobrze na urządzeniach z różną rozdzielczością, mnożnik trMultiplier jest od niej uzależniony. Pozostałe tekstury kompasu przesuwane są w podobny sposób. Aby nanieść znacznik celu nawigacji na kompas wyliczana jest wartość azymutu punktu docelowego i na jej podstawie tekstura przesuwana jest na kompasie, bądź ukrywana (jeśli jest na przykład za nami).

#### Asystent głosowy
Aplikacja posiada asystenta głosowego reagującego na hasło "Hej Click and Go", jeśli jest on włączony w jej opcjach. Po wybudzeniu asystenta wyświetlane jest okienko wprowadzania głosowego, które wyświetla mówiony do telefonu tekst. Tekst jest następnie przekazywany do odpowiedniej funkcji jako parametr. Na jego podstawie wykonywane są różne funkcje aplikacji.

Funkcja działająca w tle, ciągle wychwytująca nowy tekst i aktywująca asystenta na ustalone hasło:
```java
            public void onResults(Bundle results) {
                ArrayList <String> matches = results.getStringArrayList(speechRecognizer.RESULTS_RECOGNITION);
                if (matches != null){
                    String speech = matches.get(0);
                    if (speech.toLowerCase().contains("click and go")){
                        try {
                            getSpeech();
                        }
                        catch (ArrayIndexOutOfBoundsException e) {
                            e.printStackTrace();
                        }
                    }
                    else if (voiceAssistant) startSpeech();
                } else if (voiceAssistant) startSpeech();
            }
```
Funkcja jawnie wczytująca mowę do aplikacji po wybudzeniu asystenta:
```java
    public void getSpeech() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "pl");

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 10);
        }
        else {
            Toast.makeText(this, "Your phone doesn't support voice assistant (voice recognition).", Toast.LENGTH_SHORT).show();
        }
    }
```
Fragment funkcji onActivityResult odpowiadający za wczytywanie głosu:
```java
        if (requestCode == 10) {
            if (resultCode == RESULT_OK && data != null) {
                ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                processSpeechResult(result.get(0));
            }
            if (voiceAssistant) startSpeech();
        }
```
Funkcja processSpeechResult dokonuje parsowania mowy i wywołania odpowiedniej funkcji w aplikacji. Zaimplementowane funkcje wywoływane głosem:
- wprowadzenie punktu docelowego ("Prowadź do ...")
- kalibracja kompasu ("Skalibruj kompas")
- cofnięcie kalibracji kompasu ("Cofnij kalibrację kompasu")
- pokazanie obiektów w pobliżu ("Pokaż obiekty w okolicy")
- ukrycie minimapy ("Schowaj minimapę")
- pokazanie minimapy ("Pokaż minimapę")
- pokazanie dużej mapy ("Pokaż dużą mapę")
Podane komendy są jedynie przykładowe, asystent reaguje na te same polecenia wyrażone na kilka sposobów (np. nawiguj zamiast prowadź).


#### Wyznaczanie trasy
Przy otwieraniu dużej mapy aktualna lokalizacja przesyłana jest z klasy MainActivity przy pomocy SharedPreferences.
```java
            SharedPreferences prefs = getApplicationContext().getSharedPreferences("MyPref", getApplicationContext().MODE_PRIVATE);
                String latitude_d = prefs.getString("myLatitude", null);
                String longitude_d = prefs.getString("myLongitude", null)
```
Po załadaowaniu mapy zaznaczana jest pozycja użytkownika. W celu wyznaczenia trasy stworzone zostało pole tekstowe w którym wyszukać możemy adres, lub wybrać miejsce bezpośrednio klikając na mapę. 


```java
        mMap.setOnMapClickListener(
                new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng point) {

                        Double l1 = point.latitude;
                        Double l2 = point.longitude;
                        String latitude = l1.toString();
                        String longitude = l2.toString();

                        SharedPreferences pref = mContext.getSharedPreferences("MyPref", mContext.MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("latitude", latitude);
                        editor.putString("longitude", longitude);
                        editor.apply();

                        drawRoute(point, mMap);
                    }
                });
```

```java
            Place place = Autocomplete.getPlaceFromIntent(data);
            editText.setText(place.getAddress());
```

Mapa zostaje oznaczona markerami gdzie znajduje się użytkownik oraz miejscem docelowym, następnie wysyłane jest zapytanie o trasę. Google api zwraca trasę która jest parsowana z pliku JSON. Następnie tworzony jest polyline i nakładany na mapę.
```java
  for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                List<HashMap<String, String>> path = result.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);
                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);
                    points.add(position);
                    builder.include(position);
                }

                lineOptions.addAll(points);
```
#### Nawigacja
Każde zapytanie o trasę Google api zwraca również podpowiedzi nawigacyjne - gdzie się kierować oraz w którym miejscu.  Przy zamykaniu mapy przyciskiem 'X' lub przyciskiem wstecz telefonu
```java

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(MapsActivity.this, MainActivity.class);
        intent.putExtra("navigation_hints",navigation_hints);
        startActivity(intent);
    }
```
Po powrocie do głównego ekranu wskazówki są konwertowane przy pomocy funkcji ConvertHints do bardziej przystępnej formy, a następnie używane do nawigowania.
Każde odświeżenie lokalizacji sprawdza czy znajdujemy się w punkcie gdzie należy zmienić kierunek ruchu, w przypadku gdy tak się stanie odtwarzana jest wskazówka.


#### Historia wyszukiwania
W danych aplikacji zapisywane jest pięć ostatnich punktów docelowych wybieranych przez użytkownika. Zapisywanie dokonwyane jest w klasie MapsActivity i adresy zapisywane są jako ciągi znaków w SharedPreferences po parsingu danych z google api przy pomocy klasy DataParser. Po wejściu w historię wyszukiwania w górnej lewej części ekranu, wyświetlane jest pięć ostatnich adresów docelowych. Po wybraniu jednego z nich użytkownik jest przekierowywany do okna wyszukiwania adresu docelowego z wpisanym wybranym adresem.

Wczytywanie adresów z danych aplikacji i wyświetlanie ich na przyciskach:
```java
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("MyPref", getApplicationContext().MODE_PRIVATE);
        for(int i=0; i<5; i++) {
            buttons[i].setVisibility(View.INVISIBLE);
            String dest = prefs.getString("Destinations" + "_" + i, null);

            if(dest != null){
                buttons[i].setText(dest);
                buttons[i].setVisibility(View.VISIBLE);
            }
        }
```


#### Ustawienia
Ustawienia pozwalają użytkownikom zmieniać funkcjonalność i zachowanie aplikacji. Ustawienia mogą wpływać na zachowanie w tle, na przykład włączenie asystęta głosowego, lub mogą mieć szerszy zakres, na przykład kalibracja kampasa.

W celu stworzenia menu ustawień używamy preference, jest to opis przyciskó oraz kod w klasie:

```java
@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.settings, new SettingsFragment())
                    .commit();
        }
```

W celu odzystania danych, które preferuje użytkownik definiujemy zmienną class'y ``PreferenceManager``:

```java
settingsdPref = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
```
oraz funcje ``getBoolean``, co zwraca wartość boolową odpowiadającą odpowiednemu kluczowi. 
Naprzykład pobieramy wartość przycisku ``voiceAssistant`` jeżeli nie będzie zdefiniowanego takiego klucza to zwróć wartość ``false``:

```java
settingsdPref.getBoolean("voiceAssistant", false);
```
